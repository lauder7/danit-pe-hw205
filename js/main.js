let oUser = createNewUser();
if (oUser !== null)
    console.log(`User ${oUser.firstName} ${oUser.lastName} has login ${oUser.getLogin()}`);
else
    console.log(`No user has been created`);




function createNewUser() {
    let sFirstName = inputName("Creating new user. Enter first name");
    if (sFirstName === null) return null;

    let sLastName = inputName("Enter last name");
    if (sLastName === null) return null;

    return {
        _firstName: sFirstName,
        get firstName()         { return this._firstName  },
        set firstName(value)    { this._firstName = value },

        _lastName: sLastName,
        get lastName()          { return this._lastName   },
        set lastName(value)     { this._lastName = value  },
        
        getLogin: function ()   { return this._firstName.toLowerCase()[0] + this._lastName.toLowerCase(); }
    };
}




function inputName(sMsg) {
    let sUserName = "";

    do {
        sUserName = prompt(sMsg, sUserName);
    } while (sUserName !== null && !sUserName)

    return sUserName;
}